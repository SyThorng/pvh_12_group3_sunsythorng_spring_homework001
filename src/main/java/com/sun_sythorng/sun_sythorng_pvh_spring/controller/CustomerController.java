package com.sun_sythorng.sun_sythorng_pvh_spring.controller;


import com.sun_sythorng.sun_sythorng_pvh_spring.response.Res;
import com.sun_sythorng.sun_sythorng_pvh_spring.response.Respones;
import com.sun_sythorng.sun_sythorng_pvh_spring.response.updateRespones;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/customers")
public class CustomerController {

    ArrayList<Customer> listCustomer = new ArrayList<>();

    public CustomerController() {
        listCustomer.add(new Customer(1, "koko", "Male", 28, "PP"));
        listCustomer.add(new Customer(2, "jaja", "FeMale", 25, "PVH"));
        listCustomer.add(new Customer(3, "haha", "Male", 24, "SR"));
    }

    //Done
    @GetMapping("/getAllCustomer")
    public ResponseEntity<Respones<ArrayList<Customer>>> getAllCustomer() {
        return ResponseEntity.ok(new Respones<>("Get All Data Successfully....!!", listCustomer, "OK", LocalDateTime.now()));
    }


    //Done
    @GetMapping(path = "/SearchById/{id}")
    public ResponseEntity<Respones> getById(@PathVariable int id) {
        for (Customer customer : listCustomer
        ) {
            if (customer.id == id) {
                Respones respones1 = new Respones("This record has found successfully", customer, "OK", LocalDateTime.now());
                return ResponseEntity.ok(respones1);
            }
        }
        return ResponseEntity.notFound().build();
    }

    //Done
    @DeleteMapping(path = "/DeleteById/{id}")
    public ResponseEntity<Res> deleteCustomer(@PathVariable int id) {
        for (int i = 0; i < listCustomer.size(); i++) {
            if (id == listCustomer.get(i).id) {
                Res res = new Res("Congratulation your delete is successfully", "OK", LocalDateTime.now());
                listCustomer.remove(i);
                return ResponseEntity.ok(res);
            }
        }
        return ResponseEntity.notFound().build();
    }

    //Done
    @PutMapping(path = "/{id}")
    public ResponseEntity<Respones> updateCustomer(@PathVariable int id, @RequestBody updateRespones up1) {
        for (Customer customer : listCustomer
        ) {
            if (customer.id == id) {
                customer.setName(up1.getName());
                customer.setGender(up1.getGender());
                customer.setAge(up1.getAge());
                customer.setAddress(up1.getAddress());
                Respones respones3 = new Respones("You're update successfully", customer, "OK", LocalDateTime.now());
                return ResponseEntity.ok(respones3);
            }
        }
        return ResponseEntity.notFound().build();
    }


    //Done
    @PostMapping("/insertCustomer")
    public ResponseEntity<Respones> insertCustomer(@RequestBody updateRespones up) {

        String name = up.getName();
        String gender = up.getGender();
        String add = up.address;
        int age = up.getAge();

        int id = 0;
        int index = 0;
        for (int i = 0; i < listCustomer.size(); i++) {
            id = listCustomer.get(i).id + 1;
        }
        listCustomer.add(new Customer(id, name, gender, age, add));
        for (int i = 0; i < listCustomer.size(); i++) {
            if (id == listCustomer.get(i).id) {
                index = i;
                break;
            }
        }
        Respones respones5 = new Respones("This record was successfully created..!!!", listCustomer.get(index), "OK", LocalDateTime.now());
        return ResponseEntity.ok(respones5);
    }


    //Done
    @GetMapping(path = "/SearchByName")
    public ResponseEntity<Respones> searchByName(@RequestParam String name) {
        for (Customer customer : listCustomer
        ) {
            if (customer.getName().equals(name)) {
                Respones respones1 = new Respones("This record has found successfully", customer, "OK", LocalDateTime.now());
                return ResponseEntity.ok(respones1);
            }
        }
        return ResponseEntity.notFound().build();
    }
}
