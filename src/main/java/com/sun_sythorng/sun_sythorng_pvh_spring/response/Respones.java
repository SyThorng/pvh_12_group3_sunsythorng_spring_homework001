package com.sun_sythorng.sun_sythorng_pvh_spring.response;

import com.sun_sythorng.sun_sythorng_pvh_spring.controller.Customer;

import java.time.LocalDateTime;
import java.util.List;

public class Respones<T> {



    public String messge;
    public T data;
    public String status ;
    LocalDateTime dateTime;

    public Respones(List<Customer> listCustomer){}

    public Respones( String messge,T data, String status, LocalDateTime dateTime) {
        this.messge = messge;
        this.data = data;
        this.status = status;
        this.dateTime = dateTime;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessge() {
        return messge;
    }

    public void setMessge(String messge) {
        this.messge = messge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
