package com.sun_sythorng.sun_sythorng_pvh_spring.response;

import java.time.LocalDateTime;

public class Res {
    public String messge;
    public String status ;
   public LocalDateTime dateTime;

    public Res(String messge, String status, LocalDateTime dateTime) {
        this.messge = messge;
        this.status = status;
        this.dateTime = dateTime;
    }
}
