package com.sun_sythorng.sun_sythorng_pvh_spring;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Customer REST API", version = "1.0", description = "Welcome to Spring API"))
public class SunSythorngPvhSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(SunSythorngPvhSpringApplication.class, args);
    }

}
